---
permalink: /
---
## About me

I am a second year PhD student in algebraic topology at [The Copenhagen Centre for Geometry and Topology][uni2] 
with advisors [Robert Burklund][bob] and [Jesper Grodal][jesper]. My main interests are in stable homotopy
theory and higher algebra, with an eye towards algebraic geometry, $K$-theory and motivic homotopy theory.

## Preprints

1. ***On the deformation theory of $\mathbb{E}_\infty$-coalgebras*** *(submitted)* [arxiv link][thesis]\
We introduce a notion of formally étale coalgebras and show that they admit unique, functorial lifts
along square zero extensions of $\mathbb{E}_{\infty}$-rings. Using this, we define Weil-restriction
of coalgebras and show that it behaves analogously to the spherical Witt-vector construction. 

2. ***The algebraic K-theory of non-split tori*** *(joint with Bai, Carmeli and Juran; in preparation)*\
We construct a motivic $KGL$-based Fourier transform for sheaves of connective spectra and use it to compute the
$K$-theory spectrum of an algebraic torus $T$ in terms of
the motivic group ring of the étale delooping of the character lattice $\Lambda^\ast(T)$.

## Stuff I am thinking about

A non-exhaustive, infrequently updated list of things that are on my mind.

Power operations on arithmetic cohomology theories -- Many invariants arising in algebraic geometry can be thought of as 
taking values in a derived category with some additional bells and whistles. However, they also usually have the structure
of commutative algebras in those categories, what can we learn about a scheme from the $\mathbb{E}_{\infty}$-ring structure
on its cohomology?

Higher chromatic prismatic cohomology -- One can define a version of prismatic cohomology replacing $\delta$-rings with 
power operations over higher height Morava $E$-theory. The redshift philosophy suggests that this should not actually contain
new information, but does it? 

Has thou considered the coalgebra?

## Other writing

Expository material and notes.

**The rationalization of the $K(n)$-local sphere** [pdf][twotow]\
Notes for a talk I gave in the Topics in Topology seminar at KU.

**Galois categories and the étale fundamental group** [pdf][ba]\
A lightly edited version of bachelor thesis.

**Topological Cyclic Homology** [pdf][tc]\
My notes on $THH$, $TC$ and friends for the popular [TV-series][tcyt].


## CV and contact

My CV as of 12.06.24:
[Curriculum Vitae](/assets/documents/cv.pdf)

Feel free to reach out to me under my email:
[florian.riedel@pm.me][email]

My office is at:

Department of Mathematical Sciences\
Universitetsparken 5,\
Office 04.4.03\
2100 Copenhagen

[email]: mailto:florian.riedel@pm.me
[uni]: https://www.uni-muenster.de/MathematicsMuenster/index.shtml
[bob]: https://burklund.github.io/
[jesper]: https://web.math.ku.dk/~jg/
[thesis]: https://arxiv.org/abs/2303.12958
[uni2]:https://geotop.math.ku.dk/
[ba]: /assets/documents/ba.pdf
[tc]: /assets/documents/tc.pdf
[ant]: /assets/documents/ant.pdf
[tcyt]: https://www.youtube.com/playlist?list=PLsmqTkj4MGTB8pNGvW0iuKUFmBlOSke-C
[twotow]: /assets/documents/two_towers.pdf
