---
title: "A ridiculous definition"
date: 2023-04-13
categories:
  - blog
tags:
  - fields
---

I am quite fond of silly but correct definitions. One of the more famous ones appears in
Paolo Aluffi's wonderful textbook ``Algebra Chapter 0'':
\newline
\underline{\textbf{Joke 1.1}}~
A \emph{group} is a groupoid with a single object.
\newline
This is both funny and great pedagogy, if your goal is to get students acquainted with
category theory. The crucial thing about categories, in my opinion, is that they are both an
algebraic gadget themselves, but also a ``place'' where other (algebraic) gadgets live. Of course
one should really notice that this defines a strange 2-category of groups, with a slightly
unconventional notion of equivalence, but other than that it's quite helpful. The ridiculous
definition I want to talk about today is everything but helpful. I came across it while grading
some abstract homework a couple of years ago. Our idea for the course was, instead of
mindlessly following some standard abstract algebra textbook, we wanted to formulate Galois Theory
as an equivalence of categories
\[ \{\text{Field extensions between}~ L/K \} \xrightarrow{\sim} \{\text{finite transitive}~
  G\text{-sets}\}\]
for $L/K$ a Galois extension and $G= \mathrm{Aut}(L/k)$. Naturally this caused a lot of confusion
among our poor undergrads and on the homework sheets one of them found themselves fumbling around with functors
\[F: \mathrm{Set} \to \mathrm{Field}.\]
While this was a bit of a fool's errand we had not intended, I immediately asked myself:
\textit{Wait, what would such a functor look like? Are there even any?}
The answer to these questions is my silly definition of the day:
\newline
\underline{\textbf{Definition}}~
A field extension is a functor $F: \mathrm{Set}\to \mathrm{Field}$.
\newline
The thing is that both of these categories are very special categories, but in completely
different ways. Indeed, any map of sets $\ast \to S $ admits a left-inverse given by the terminal
map $S \to \ast$. Thus, given a functor $F$ we obtain field extensions
\[ F(\ast) \to F(S) \to F(\ast).\]
This forces both maps to be isomorphisms and so the only ``real'' choice we have in defining such
a functor is where to send the \emph{empty} map
\[ \emptyset \to \ast,\]
which is precisely the datum of a field extension $F(\emptyset)\to F(\ast)$. There is some ambiguity
here: If we want to, we can define $F(\ast) \to F(S)$ to be some isomorphism that is not the identity.
However, the space of such choices is \emph{contractible}, which is why I call it ``not a real
choice''. To be more precise, restricting along the inclusion $\Delta^{1}\hookrightarrow \mathrm{Set}$
induces an equivalence of categories
\[\mathrm{Fun}(\mathrm{Set}, \mathrm{Field}) \xrightarrow{\sim} \mathrm{Fun}(\Delta^{1},\mathrm{Field}).\]
The right hand category is now really the category which has objects given by field extensions
$K \to L$ and morphisms given by commutative diagrams
\[
\begin{array}{ccc}
  L & \to & L^{\prime}\\
  \uparrow & & \uparrow \\
  K & \to & K^{\prime}.
\end{array}
\]
So, up to equivalence, our definition  gives the ``correct'' category of field extensions
and that's really all we could ask for. Proving this amounts to showing that for any such functor
$F$ and any set $S$ the action of $Aut(S)$ on $F(S)$ is trivial, which I leave as an exercise to the
reader. Is there anything we can learn from this? I don't think so honestly and I strongly advise
against even mentioning this fact in any course on abstract algebra.
