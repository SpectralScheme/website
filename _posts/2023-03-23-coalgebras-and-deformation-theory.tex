---
title: "Coalgebras and deformation theory (work in progress)"
date: 2023-03-24
categories:
  - blog
tags:
  - coalgebras
  - deformation theory
---

Here's a vague question: Given a space $X$ what's the difference between the $\mathbb{F}_{p}$-valued chains
$C_{\ast}(X;\mathbb{F}_{p})$ and the $p$-completed suspension spectrum $(\Sigma^{\infty}_{+}X)^{\wedge}_{p}$?.
The suspension spectrum is obviously a stronger invariant for $X$ as we can recover the chains as
the base change
$$ C_{\ast}(X;\mathbb{F}_{p})\simeq (\Sigma^{\infty}_{+}X)^{\wedge}_{p}\otimes_{\mathbb{S}^{\wedge}_{p}} \mathbb{F}_{p}. $$
However, for a finite space $X$, they turn out to be equivalent data, so long as we consider them as
\textit{coalgebras}! In fact this likely true for non-finite spaces as well, however we only have
a proof for the finite case as of now, so we make that assumption in what follows.
\section{What's the deal with coalgebras?}
But let's back up a bit: What even is a coalgebra, and where does this additional structure come from?
So you may have heard of a coalgebra over a monad, this is \textit{not} what we are talking about
here, although the notions are related. What we mean by a coalgebra is the following:
Given a symmetric monoidal category $\mathcal{C}$, a coalgebra $A\in \mathcal{C}$ is an object equipped with
a comultiplication
$$\Delta_{A}: A \to A \otimes A$$
and counit
$$ \eta_{A}: A \to 1_{\mathcal{C}}$$
map which satisfy relation \emph{coassociativity} and \emph{counitality} relations. For our purposes
we also want all coalgebras to be \emph{cocommutative} and we leave this implicit from here on out.
Instead of drawing the diagrams, let me just summarize this by saying that a coalgebra is the same
as an algebra in the opposite category $\mathcal{C}^{\mathrm{op}}$. A map of coalgebras $A \to B$ is a map of
underlying objects wchich is \emph{comultiplicative}, hence the category of coalgebras is given
by
$$ \mathrm{cCAlg}(\mathcal{C}) := \mathrm{CAlg}(\mathcal{C}^{\mathrm{op}})^{\mathrm{op}}.$$
Coalgebras are often considered in the category of commutative rings $\mathrm{CRing}$ because what
people really want to talk about group schemes, and affine schemes happen to be the opposite of rings.
That is, coalgebras appear, but people are not typically ``really'' interested in them in a conceptual way,
mainly because comultiplication is a much less natural notion than multiplication. However, coalgebras
play a crucial if somewhat hidden role in homotopy theory:
Any topological space $X$ actually comes equipped with a natural coalgebra structure
given by the diagonal map
$$ X \xrightarrow{\Delta} X \times X$$
and the terminal map
$$ X \to \ast.$$
This may seem like a trivial structure, and in fact it is! Once can show that if $\mathcal{C}$
is \emph{cartesian monoidal}, that is the tensor product is given by the product, then
we have an equivalence
$$ \mathrm{cCAlg}(\mathcal{C}) \simeq \mathcal{C},$$
so these maps are really no datum at all. Things get interesting once we apply homology
however. The Eilenberg-Zilber Theorem says that we have a homotopy equivalence
$$ C_{\ast}(X \times X) \simeq C_{\ast}(X) \otimes C_{\ast}(X)$$
between the singular chains on the product space $X$ and the tensor product of the singular chains
on $X$. This means that the trivial coalgebra structure on $X$ induces a not-so trivial coalgebra
structure on the homology! The way we usually learn about this is via the cup product on the cohomology:
If $\mathcal{C}$ is closed monoidal, then the dual of a coalgebra $A$ given by
$$ A^{\vee}= \mathcal{C}(A, 1_{\mathcal{C}})$$
inherits a natural algebra structure. This is precisely where the multiplication on singular
homology comes from. But hold on, the duality functor
$$ (\cdot)^{\vee}:\mathrm{cCAlg}(\mathcal{C}) \to \mathrm{CAlg}(\mathcal{C})$$
is usually \emph{not} fully faithful. This means that the cohomology ring is actually a worse approximation
to $X$ than the homology coalgebra, as some information is lost by dualizing. Before we continue,
let us first drop the 1-categorical charade. This entire story makes perfect
sense for a symmetric monoidal $\infty$-category $\mathcal{C}$ where we can talk about $\mathbb{E}_{\infty}$-
coalgebras in $\mathcal{C}$. The good news is that all definitions remain the same, meaning I was
secretly talking about $\infty$-categories this entire time. This is really the appropriate
framework if we want to talk about homology as a coalgebra. In fact, we do not even need to
conjure the Eilenberg-Zilber theorem to understand what is happening here. Let $\mathcal{S}$ denote
the $\infty$-category of spaces and for any $\mathbb{E}_{\infty}$-ring $R$ write $\mathrm{Mod}_{R}$ for
the $\infty$-category of $R$-modules. For $\mathbb{S}$ this is the category of spectra $\mathrm{Sp}$
and for an ordinary ring it is the derived category $D(R)$. Then the $R$-homology can be
thought of as a functor
\[ \mathcal{S} \to \rm{Mod}_{R} \qquad \]
which takes a space $X$ to the colimit
$$ \mathrm{colim}_{X}R \simeq \Sigma^{\infty}_{+}X \otimes_{\mathbb{S}} R$$
which denote by $R[X]$. If $R$ is an ordinary ring we recover the $R$-homology groups as the
homotopy groups
$$ \pi_{\ast} R[X] = H_{\ast}(X;R).$$
The notation $R[X]$ is supposed to be evocative of the group ring construction, precisely because
it behaves as such! The functor $R[\cdot]$ is strong symmetric monoidal: For any
two spaces $X, Y$ we have
\begin{align*}
 R[X\times Y] = \mathrm{colim}_{X\times Y} R &\simeq \mathrm{colim}_{X}\mathrm{colim}_{Y}(R\otimes_{R}R)\\
                                   &\simeq \mathrm{colim}_{X}R \otimes_{R} \mathrm{colim}_{Y}R\\
  &\simeq R[X]\otimes_{R}R[Y]
\end{align*}
because in the tensor product commutes with colimits in both variables separately. This is actually
just the Eilenberg-Zilber Theorem cast in modern language and greater generality. Thus, just like
with the group ring functor, if we start with an $\mathbb{E}_{\infty}$-monoid $X$ we get an
$\mathbb{E}_{\infty}$-algebra $R[X]$. More crucially for us however, since $X$ is always
naturally a coalgebra in $\mathcal{S}$, the $R$-homology $R[X]$ inherits a natural coalgebra
structure as well.
So it makes sense to talk about $(\Sigma^{\infty}_{+}X)^{\wedge}_{p} = \mathbb{S}[X]^{\wedge}_{p}$ and
$C_{\ast}(X;\mathbb{F}_{p}) = \mathbb{F}_{p}[X]$ as coalgebras and compare them.
We claim that $\mathbb{S}[X]^{\wedge}_{p}$ is the essentially unique lift of $\mathbb{F}[X]$
to a $p$-complete $\mathbb{E}_{\infty}$-coalgebra over $\mathbb{S}_{p}^{\wedge}$.
\section{Deformation theory}
So what does this have to do with deformation theory? Classically, deformation theory is really the study
of how to lift stuff along square zero extensions. A map of rings $R \to S$ is called a
\emph{square zero extension} if the kernel $I$ satisfies $I^{2} = 0$. So for example,
for each $n$ the map
$$ \mathbb{Z}/p^{n+1} \to \mathbb{Z}/p^{n}$$
is a square zero extension with fiber $\mathbb{Z}/p$. Frequently people considering an iteration of this problem
and ask how to lift say an algebra or an elliptic curve all the way from $\mathbb{F}_p$ to the $p$-adics
$\mathbb{Z}_{p}= \lim_{n}\mathbb{Z}/p^{n}$. I want to explain how homotopy theory can already help us understand these classical
problems better. Let $R \to S$ be a square zero extension of ordinary rings. Then we have a fiber sequence
in the derived category $D(R)$
\[I \to R \to S.\]
Since $D(R)$ is stable, we can recover the extension $R\to S$ as the fiber of the map $S \to \Sigma I$ where $\Sigma I$ denotes
the suspension of $I$. If we model $D(R)$ using chain complexes, this amounts to shifting $I$ to the right by one.
Hence the extension is classified by a point
\[\delta \in \pi_{0}\mathrm{Map}_{D(R)}(S , \Sigma I)= \mathrm{Ext}_{R}^{1}(S, I)\]
This raises the question, is it possible to tell from the map $ S \to \Sigma I$ that we started with a square zero extension?
The answer is yes, this is precisely the case if $\delta: S \to \Sigma I$ assembles to map of $\mathbb{E}_{\infty}$-rings
\[ S \to S \oplus \Sigma I\]
which is given by the identity of the first factor. Here $S \oplus \Sigma I$ is the split square zero extension with fiber
$\Sigma I$ which we will define in a moment. We call such maps $\delta$ \emph{derivations} and write
\[ \mathrm{Der}(S, \Sigma I):= \mathrm{Map}_{\mathrm{CAlg}_{/S}}(S, S \oplus \Sigma I)\]
for the space of derivation $S \to \Sigma I$. If $\delta$ is a derivation we can recover $R$ (as a ring!) as the pullback
of $\mathbb{E}_{\infty}$-rings
\[
\begin{array}{ccc}
  R & \longrightarrow & S\\
  \downarrow &  & \downarrow\\
  S & \rightarrow & S \oplus \Sigma I,
\end{array}
\]
where the vertical map $S\oplus \Sigma I$ is the trivial section $(\mathrm{id}, 0)$. Thus, we can think of $S\oplus \Sigma I$
as a classifying object for square zero extensions of $S$ with fiber $I$ with "universal bundle"
given  by the zero section $S \to S\oplus \Sigma I$ .In this sense, a general square zero extension
can be thought of as a ``twisted'' version of a split extension. We can spin this analogy further:
In fact, in the category $\mathrm{CAlg}_{/S}$ of $\mathbb{E}_{\infty}$-rings lying over $S$
we have that $\Omega^{n} (S\oplus \Sigma^{n}I) \simeq S \oplus I$, so this defines
a grouplike $\mathbb{E}_{\infty}$-monoid in $\mathrm{CAlg}_{/S}$ with delooping given by
$B (S \oplus I) \simeq S \oplus \Sigma I$. This observation can be upgraded to an equivalence of categories
\[ D(S) \xrightarrow{\sim} \mathrm{Sp}(\mathrm{CAlg}_{/S})\]
so we want to think of $S\oplus I$ as the composition of this equivalence with the infinite loop space functor
\[\Omega_{S}^{\infty}: \mathrm{Sp}(\mathrm{CAlg}_{/S}) \to \mathrm{CAlg}_{/S}.\]
(Be careful not to confuse this with the underlying infinite loop space we get from the composition
\[ D(S)\hookrightarrow \mathrm{Sp}= \mathrm{Sp}(\mathcal{S})\xrightarrow{\Omega^{\infty}} \mathcal{S} \]
these are very different things.) In homotopy land, we will run this train of thought backwards and
define map of $\mathbb{E}_{\infty}$-rings $R \to S$ to be a zero extension with fiber $I\in \rm{Mod}_{R}$ if it is
pulled back from a classifying map $S \to S \oplus \Sigma I$ which is the identity on $S$.
In fact, lifting along such square zero extensions becomes hugely important in homotopy theory:
 If $R$ is a connective $\mathbb{E}_{\infty}$-ring then the truncation maps
\[ \tau_{\leq n+1} R \to \tau_{\leq n} R \]
are square zero extensions with fiber $\Sigma^{n}\pi_{n}R$. For the case $R = \mathbb{S}_{p}^{\wedge}$ this tells us that
the $p$-completed sphere is a limit of square zero extensions of $\mathbb{Z}_{p} = \pi_{0}\mathbb{S}_{p}^{\wedge}$!
So passing from $\mathbb{F}_{p}$ to $\mathbb{S}_{p}^{\wedge}$ is really fundamentally a deformation theoretic question
if you go via the ``middle man'' $\mathbb{Z}_{p}$.\\
So this is already a pretty nice story, but I said that we want to lift ``stuff'' against square zero extensions
(coalgebras, initially), so what do I mean by ``stuff''? If we want to be a little more suggestive we should
say ``stuff parameterized by $\mathbb{E}_{\infty}$-rings'', which really means functors
\[ X:\mathrm{CAlg} \to \mathcal{S}.\]
From now on we want all rings and modules to be connective, so I will assume this without further mention. You
may consider this a technicality, but it is necessary for the formalism to work. Such a functor $X$ is sometimes
called a (derived) moduli problem, where we think of $X$ as parameterizing some sort of geometric object and
people frequently want to know whether $X$ is representable. In our case we ultimately want to take $X$ to be the functor
which takes $R$ to the $\infty$-category $\mathrm{cCAlg}_{R}$. Since this has absolutely no chance of being representable,
we won't consider those questions here. However, if you're paying attention you may have noticed that I said
$X$ takes values in spaces, but $\mathrm{cCAlg}_{R}$ is an $\infty$-category. This is often swept under the rug by
considering the underlying $\infty$-groupoid and taking care of the non-invertible maps separately. There is actually
a formal reason why this works: Any $\infty$-category $\mathcal{C}$ can be recovered from the simplicial space
\[ \mathcal{C}^{\Delta^{\bullet}} = \mathrm{Map}_{\mathrm{Cat}_{\infty}}( \Delta^{\bullet}, \mathcal{C} )\]
so we can break down understanding $\mathcal{C}$ into understanding the spaces of $n$-simplices in $\mathcal{C}$ one
at a time. This may seem hopeless  but the trick is that for many problems we only ever need to consider
$\mathcal{C}^{\Delta^{0}}$, i.e.~the underlying $\infty$-groupoid of $\mathcal{C}$, and $\mathcal{C}^{\Delta^{1}}$, the space of maps in $\mathcal{C}$.
(It is a frequent observation that $\infty$-category theory really ``feels'' just like doing fancy 2-category
theory. This is formally captured by the notion of an $\infty$-cosmos introduced by Riehl and Verity, which really
says that a ``theory of $\infty$-categories'' is just a kind of simplicial set with an underlying homotopy
2-category, and many constructions only really refer to that underling homotopy category, but I digress.)
Notice that, given a derived moduli problem, you can always get an ``ordinary'' one by restricting to the
ordinary category of commutative rings and applying $\pi_{0}$. Moreover, many ordinary moduli problems admit an
enhancement to a derived, so we can try to understand them in homotopy land and the apply $\pi_{0}$ to get a
statement that non-homotopy theorists care about as well.\\
We say that a functor $X: \mathrm{CAlg} \to \mathcal{S}$ is \textit{cohesive} if commutes with pullbacks
\[
\begin{array}{ccc}
  R^{\prime}&\to &S^{\prime}\\
  \downarrow & & \downarrow \\
  R & \to &S
\end{array}
\]
for which the map $\pi_{0}R \to \pi_{0}S$ is surjective. Notice that, if $R \to S$ is a square zero extension with
fiber $I$, then the pullback diagram we discussed above
\[
\begin{array}{ccc}
  R & \longrightarrow & S\\
  \downarrow &  & \downarrow\\
  S & \rightarrow & S \oplus \Sigma I,
\end{array}
\]
has this property (since we assumed everything is connective, I told you it was important!). This means that
if we apply $X$ we get a pullback diagram
\[
  \begin{array}{ccc}
  X(R)& \to & X(S)\\
  \downarrow & & \downarrow\\
  X(S) & \to & X(S \oplus \Sigma I)
  \end{array}
\]
Thus we can describe the values of $X$ on arbitrary square zero extensions if we know
what it does on the split ones. Note that even if we start with ordinary rings $S$
and $R$ and a functor taking values in $\mathrm{Set}\subseteq \mathcal{S}$ we \emph{need} to go derived
to make sense of this statement, because the ring $S \oplus \Sigma I$ doesn't exist in oridnary
land. \\
Given some $A \in X(S)$ what we really want to understand is the space of lifts of $A$
to a point $A^{\prime} \in X(R)$, that is, we want to compute the fiber
\[ X^{R}_{A}:= \mathrm{fib}_{A}(X(R)\to X(S)).\]
Let us write $A^{0}= X((\mathrm{id}, 0))(A)$ and $A^{\delta}= X((\mathrm{id}, \delta))(A)$ for the ``tirival''
lifts to $S \oplus \Sigma I$ obtained from the two different sections. Taking fibers gives another pullback diagram
\[
\begin{array}{ccc}
  X^{R}_{A} & \to & \ast\\
  \downarrow & & \downarrow\\
  \ast & \to & X_{A}^{S \oplus \Sigma I}.
\end{array}
\]
I claim that this is the only diagram you will ever need to understand deformation theory! Well not quite,
but it tells us a ton, so let us take some time to unravel what this means. First of all, this exhibits
$X^{R}_{A}$ as the space of paths between the two points $A^{\delta}, A^{0}\in X^{S\oplus \Sigma I}_{A} $. Explicitly
this means that a lift of $A$ to $R$ is given by a path $A^{0}\to A^{\delta}$ lifting the identity $A \to A$.
Of course there may be no such paths, which just tells us that $X^{R}_{A}$ is empty! However if it is
non-empty then this space of paths is a torsor under space of loops based at $A^{0}$.
Taking homotopy groups,
this tells us that there is a obstruction class sitting in $\pi_{0}X_{A}^{S \oplus \Sigma I}$ which vanishes iff $A$
admits a lift to $R$. If such a lift exists, then the set of isomorphism classes of lifts is a torsor
under $\pi_{1}(X_{A}^{S \oplus \Sigma X}, A^{0})$ and thus non-canonically isomorphic to it. In fact, if we plug in
$R= S \oplus I$ into our all important diagram we see that $\Omega_{A^{0}}X_{A}^{S \oplus \Sigma I} \simeq X_{A}^{S\oplus I}$, so
if this obstruction class vanishes, we even get a (non-canonical) equivalence of spaces
\[X_{A}^{S \oplus I} \simeq X_{A}^{R}.\]
Thus, if we can lift along a given square zero extension \emph{at all}, then the lifts necessarily
behave the same as if we were lifting along a split square zero extension with the same fiber.
What's more, the spaces $X_{A}^{S \oplus \Sigma^{n} I}$ actually assemble to a spectrum which we denote by
$T_{X_{A}}^{I}$. This packages everything you could want to know about lifting $A$ against
square zero extensions with fiber given by some shift of $I$ into one convenient spectrum.
For $I=S$ this is called the \textit{tangent complex} of $X$ at the point $A$.
Before we run away too far with this story, let me give you some examples:\\
Suppose $X = \mathrm{Spec}(k)$ where $k$ is some ring. Then a point in $\varphi \in X(S)$ is a map
\[ \varphi: k \to S\]
and $X^{R}_{\varphi}$ is the space of lifts of $\varphi$ to a diagram
\[
\begin{array}{ccc}
  & & R\\
  & \nearrow & \downarrow \\
  k & \xrightarrow{\varphi} & S
\end{array}
\]
Then from our theory we get that such a lift exists if and only if we can find a homotopy identifying
the two compositions
\[ k \xrightarrow{(\varphi, \delta)} S \oplus \Sigma I\]
\[ k \xrightarrow{(\varphi, 0)} S \oplus \Sigma I,\]
while keeping $\varphi$ fixed. By definition, this a nullhomotopy of $\varphi \circ \delta$ in the space
\[ \mathrm{Der}(k, \Sigma I) = \mathrm{Map}_{\mathrm{CAlg}_{/S}}(k, S \oplus \Sigma I).\]
Moreover, if such a homotopy exists, then the space of lifts is a torsor
under the space of lifts in the diagram
\[
\begin{array}{ccc}
  & & S \oplus I\\
  & \nearrow & \downarrow \\
  k & \xrightarrow{\varphi} & S.
\end{array}
\]
which is precisely the space of derivations $\mathrm{Der}(k, I)$. This last bit you may have heard
before in ordinary land, you can simply check that the difference of any two such lifts is an ordinary
derivation, that is an additive map satisfying the Leibniz rule. But not only have we established
this fact in much greater generality now, but also we have also found that the obstruction against
the existence of lifts also lives in a space of derivations, if we allow ourselves to shift the module.
In fact the functor
\[ \mathrm{Mod}_k \to \mathcal{S} \qquad M \mapsto \mathrm{Der}(k, M) \]
turns out to be representable by a module $L_{k}$ called the \emph{cotangent complex} of $k$.
with this in mind we see that there is an obstruction class
\[ [\varphi] \in \mathrm{Ext}^{1}_{k}(L_{k}, I) = \pi_{0}\mathrm{Der}(k, \Sigma I) \]
and if this vanishes the space of lifts is a torsor under the abelian group
\[\mathrm{Hom}_{k}(L_{k}, I) = \pi_{0}\mathrm{Der}(k, I) \]
To see whether you're following along, I would advise you to convince yourself that in this setup
we have an equivalence
\[ \mathrm{Mod}_{k}(L_{k}, I) \simeq T_{X_{\varphi}}^{I}.\]
This is the justification for the names cotangent and tangent complex, with the caveat that over an
arbitrary $\mathbb{E}_{\infty}$-ring neither of these are necessarily described by actual chain complexes.

% One of the classic questions of deformation theory
% is this, given a polynomial $f\in \mathbb{Z}[t]$, and an $x$ solving the equation $f(x)=0$ modulo
% $p^{n}$, how many $x^{\prime}$ exist solving this equation modulo $p^{n+1}$? This is equivalent to
% providing a lift of the map
% \[ x:\mathbb{Z}[t]/f \to \mathbb{Z}/p^{n}\]
% to $\mathbb{Z}/p^{p^{n}+1}$. Once can compute that the difference of two such lifts gives a \emph{derivation},
% that is an additive map
% \[ \delta: \mathbb{Z}[t]/f \to p^{n}\mathbb{Z}/p^{n+1}\]
% satisfying the Leibniz rule
% \[ \delta(ab)= a\delta(b) + \delta(a)b.\]
% The set of derivations is corepresented by an abelian group called the \emph{module of Kähler differentials}
% which is denoted $\Omega_{\mathbb{Z}[t]/f}^{1}$. Putting this together we can say that the space of lifts of $x$ along the
% square zero extension $\mathbb{Z}/p^{n+1}\to \mathbb{Z}/p^{n}$ is either empty or a torsor under the group
% \[ \mathrm{Hom}_{\mathbb{Z}}(\Omega_{\mathbb{Z}[t]/f}^{1}, p^{n}\mathbb{Z}/p^{n+1}).\]
% Since the action of $\mathbb{Z}$ on the right hand term factors through $\mathbb{F}_{p}$, this is also given by
% \[ \mathrm{Hom}_{\mathbb{F}_{p}}(\Omega_{\mathbb{Z}[t]/f}^{1} \otimes \mathbb{F}_{p}, \mathbb{F}_{p} )\]
% where we have used that, as a module $p^{n}\mathbb{Z}/p^{n+1}$ is isomorphic to $\mathbb{F}_p$. The Kähler differentials
% can be explicitly computed by the formula
% \[ \Omega^{1}_{\mathbb{Z}[t]/f} \simeq \mathbb{Z}[t]/ (f + f^{\prime}),\]
% which means that $\Omega^{1}_{\mathbb{Z}[t]/f}\otimes \mathbb{F}_{p}$ vanishes precisely if $f$ and its formal derivative $f^{\prime}$
% are coprime modulo $p$! So if this is the case, all lifts are necessarily unique, if they exist. This is precisely
% the uniqueness part of Hensel's Lemma. One can also show that lifts always exist under this condition using this
% formalism, meaning we can lift solutions all the way from $\mathbb{F}_{p}$ to $\mathbb{Z}_{p}$.
% This normally the part where people start ad-hoc introducing some cohomology groups and doing cocycle
% computations, but I will refrain from doing so. Let me say instead that, while this is a great story, it's entirely
% unclear how to adapt it to similar but different problems. Say for example I don't want to lift solutions to polynomial
% equations from $\mathbb{F}_{p}$ to $\mathbb{Z}_{p}$, but rather an algebra or an elliptic curve.



% This ``conjugation'' with the opposite category makes coalgebras behave kind of funky. Taking the
% opposite category is an involution and hence an equivalence $\mathrm{Cat}\xightarrow{\sim} \mathrm{Cat}$,
% which preserves many but not all properties. Some things from algebras also carry over, but become
% weird: For example, if $\mathcal{C}$ is $\kappa$-presentable, then $\mathrm{cCAlg}(\mathcal{C})$ is presentable as well, but
% for some
